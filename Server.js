const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const dotenv = require('dotenv')

dotenv.config({ path: `./config/.env.${process.env.NODE_ENV}` })

const app = express()

// Body parser middleware
app.use(bodyParser.json({ limit: '10mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))

app.use(morgan('dev'))

// Handle CORS error
app.use(cors('Access-Control-Allow-Origin', '*'))

// Serve static Image files
app.use('/uploads', express.static(__dirname + '/uploads'))

// Set Template Engine
app.set('view engine', 'ejs')

// //Block 304: Not Modified
// app.get('/*', function(req, res, next){
//   res.setHeader('Last-Modified', (new Date()).toUTCString());
//   next();
// });

// Routes
app.get('/', function (req, res) {
  console.log('Server active')
  res.send('Server active')
})

require('./routes/UserRoute')(app) // Route for User actions
// require('./routes/RadioLanguageRoute')(app) // Route for radio Languages routes

// Handle route not found
app.use((req, res, next) => {
  const error = new Error('Routes Not Found')
  error.status = 404
  next(error)
})

// Error handling middleware
app.use((error, req, res, next) => {
  res.status(error.status || 500).json({
    success: false,
    message: error.message,
    // stack: (error.stack),
  })
})

// Handle unhandled rejections and exceptions
process.on('unhandledRejection', (reason, promise) => {
  console.log('Unhandled rejection at', promise, `reason: ${reason}`)
  process.exit(1)
})

process.on('uncaughtException', error => {
  console.log('Unhandled Exception:', error.message)
  process.exit(1)
})

const port = process.env.PORT || 5000
app.listen(port, () => console.log('Server is running on port: ' + port))
