const {
  filterExt,
  limitPagination,
  handleFileUploadError,
} = require('../middleware/RequestHandler')
const user = require('../controller/UserController')
const multer = require('multer');



const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/'); // Store profile pics in 'uploads/' directory
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + '-' + file.originalname); // Generate a unique filename
  },
});




const upload = multer({ storage });



module.exports = app => {
  app.post('/createUser',upload.single('profilePic'),user.createUser);
  app.get('/getAllFathers',user.getAllFathers);
  app.get('/getAllMothers',user.getAllMothers);
  app.get('/familytree/:person_id',user.getFamilyTree);
  app.get('/getAllUsers',user.getAllUsers);
}
