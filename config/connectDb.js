const mysql = require('mysql2/promise')

db_name = process.env.DB_DATABASE || 'adhouzz'

const pool = mysql.createPool({
  host:'localhost',
  port:3306,
  user:'root',
  password:'',
  database:'familytree',
  connectionLimit: 10,
})
// Check connection status
const checkConnection = async () => {
  try {
    const connection = await pool.getConnection()
    const [row] = await connection.query('SHOW TABLES IN adhouzz')
    if (row.length) {
      console.log('Database connected')
    } else {
      console.log('Issue connecting tables')
    }
  } catch (err) {
    // console.log('Issue connecting DB: ',JSON.stringify(err))
    if (err.code === 'ECONNREFUSED') {
      console.log(process.env.DB_HOST )
      console.log('Sql server is offline')
      setTimeout(checkConnection, 20000)
    } else if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.log('Unexpected Connection lost')
      handleDisconnect()
    } else {
      console.log('error when connecting to db:', err)
      setTimeout(checkConnection, 20000)
    }
  }
}
checkConnection()

module.exports = pool
