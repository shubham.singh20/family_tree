const fs = require('fs')
const md5 = require('md5')
const pool = require('../config/connectDb')
const queries = require('../helpers/RawQueries')
const errorMessage = require('../helpers/ErrorMessage')
// Load input validation
const validateParamId = require('../middleware/ValidateParamId')
const validateLoginInput = require('../middleware/ValidateLoginInput')
const validateRegisterInput = require('../middleware/ValidateRegisterInput')

// exports.createUser = async (req, res) => {
//   console.log("req",req.body);
//   const { firstName, lastName,gender, fatherName, motherName,city,state,email,password } = req.body;
//   const connection = await pool.getConnection();
//   const sql = 'INSERT INTO person (first_name,last_name,gender,father_name,mother_name,city,state,email,password) VALUES (?, ?, ?, ?,?,?,?,?,?)';

//   try {
//     const result = await connection.query(sql, [firstName, lastName,gender, fatherName, motherName,city,state,email,password]);
//     console.log("result",result)
//     res.status(201).json({ message: 'User created successfully' });
//   } catch (err) {
//     res.status(500).json({ error: 'unable to crate user'  });
//   } finally {
//     connection.release();
//   }
// };

exports.getAllFathers = async (req, res) => {
  const connection = await pool.getConnection();
  const sql = 'select * from person where gender = "male" ';

  try {
    const [result] = await connection.query(sql);
    console.log("result",result)
    res.status(200).json({ status: "success", data: result });
  } catch (err) {
    res.status(500).json({ error: 'unable to crate user'  });
  } finally {
    connection.release();
  }
};



exports.getAllMothers = async (req, res) => {
  const connection = await pool.getConnection();
  const sql = 'select * from person where gender = "female" ';

  try {
    const [result] = await connection.query(sql);
    console.log("result",result)
    res.status(200).json({ status: "success", data: result });
  } catch (err) {
    res.status(500).json({ error: 'unable to crate user'  });
  } finally {
    connection.release();
  }
};



exports.createUser = async (req, res) => {
  const {
    firstName,
    lastName,
    gender,
    fatherName,
    motherName,
    city,
    state,
    email,
    password,
    father_id,
    mother_id,
  } = req.body;

  const profilePic = req.file.path;

  const connection = await pool.getConnection();

  try {
    await connection.beginTransaction(); // Start a transaction

    // Insert the new user into the Person table
    const insertUserSql =
      'INSERT INTO Person (first_name, last_name, gender, father_name, mother_name, city, state,img, email, password) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?)';
    const userResult = await connection.query(insertUserSql, [
      firstName,
      lastName,
      gender,
      "ss",
      "mj",
      city,
      state,
      profilePic,
      email,
      password,
    ]);

    // Get the ID of the newly created user
    const userId = userResult[0].insertId;

    if (father_id && mother_id) {
      // Insert the relationship into the Relationship table
      const relationshipType = gender === 'male' ? 'son' : 'daughter';
      const insertRelationshipSql =
        'INSERT INTO relationship (child_id, parent_id, relationship_type) VALUES (?, ?, ?)';
      await connection.query(insertRelationshipSql, [userId, father_id, relationshipType]);
      await connection.query(insertRelationshipSql, [userId, mother_id, relationshipType]);
      await connection.query(insertRelationshipSql, [mother_id, father_id, 'spouse']);
      await connection.query(insertRelationshipSql, [father_id, mother_id, 'spouse']);
      // await connection.query(insertRelationshipSql, [userId, mother_id, relationshipType]);
    }

    await connection.commit(); // Commit the transaction
    res.status(201).json({ message: 'User created successfully' });
  } catch (err) {
    await connection.rollback(); // Rollback the transaction in case of an error
    console.error('Error creating user and relationships:', err);
    res.status(500).json({ error: 'Unable to create user and relationships' });
  } finally {
    connection.release();
  }
};

// exports.getFamilyTree = async (req, res) => {
//   const personId = req.params.person_id;

//   const connection = await pool.getConnection();

//   try {
//     // Retrieve the person's details
//     const getPersonSql = 'SELECT * FROM Person WHERE person_id = ?';
//     const [personResult] = await connection.query(getPersonSql, [personId]);

//     if (personResult.length === 0) {
//       res.status(404).json({ error: 'Person not found' });
//       return;
//     }

//     const person = personResult[0];

//     // Retrieve the person's parents
//     const getParentsSql = `
//       SELECT P.person_id, P.first_name, P.last_name, P.gender
//       FROM Relationship AS R
//       INNER JOIN Person AS P ON R.parent_id = P.person_id
//       WHERE R.child_id = ? AND (R.relationship_type = 'son' OR R.relationship_type = 'daughter')
//     `;
//     const [parentsResult] = await connection.query(getParentsSql, [personId]);

//     // Retrieve the person's children
//     const getChildrenSql = `
//       SELECT P.person_id, P.first_name, P.last_name, P.gender
//       FROM Relationship AS R
//       INNER JOIN Person AS P ON R.child_id = P.person_id
//       WHERE R.parent_id = ? AND (R.relationship_type = 'son' OR R.relationship_type = 'daughter')
//     `;
//     const [childrenResult] = await connection.query(getChildrenSql, [personId]);

//     // Retrieve the person's spouse
//     // const getSpouseSql = `
//     //   SELECT P.person_id, P.first_name, P.last_name, P.gender
//     //   FROM Relationship AS R
//     //   INNER JOIN Person AS P ON R.parent_id = P.person_id OR R.child_id = P.person_id
//     //   WHERE R.parent_id = ? AND R.relationship_type = 'spouse'
//     // `;

//     const getSpouseSql = `select child_id from relationship where parent_id = ? AND relationship_type='spouse'`;
//     let [spouseResult] = await connection.query(getSpouseSql, [personId]);
//     console.log("spouseReult",spouseResult[0].child_id);
//     [spouseResult] = await connection.query(`select * from person where person_id= ${spouseResult[0].child_id}`);

//     // Construct the family tree object
//     const familyTree = {
//       person,
//       parents: parentsResult,
//       children: childrenResult,
//       spouse: spouseResult[0] || null, // Use null if no spouse is found
//     };

//     res.status(200).json(familyTree);
//   } catch (err) {
//     console.error('Error retrieving family tree:', err);
//     res.status(500).json({ error: 'Unable to retrieve family tree' });
//   } finally {
//     connection.release();
//   }
// };


// exports.getFamilyTree = async (req, res) => {
//   const personId = req.params.person_id;

//   const connection = await pool.getConnection();

//   try {
//     // Retrieve the person's details
//     const getPersonSql = 'SELECT * FROM Person WHERE person_id = ?';
//     const [personResult] = await connection.query(getPersonSql, [personId]);

//     if (personResult.length === 0) {
//       res.status(404).json({ error: 'Person not found' });
//       return;
//     }

//     const person = personResult[0];

//     // Retrieve the person's parents
//     const getParentsSql = `
//       SELECT P.person_id, P.first_name, P.last_name, P.gender
//       FROM Relationship AS R
//       INNER JOIN Person AS P ON R.parent_id = P.person_id
//       WHERE R.child_id = ? AND (R.relationship_type = 'son' OR R.relationship_type = 'daughter')
//     `;
//     const [parentsResult] = await connection.query(getParentsSql, [personId]);

//     // Retrieve the person's children
//     const getChildrenSql = `
//       SELECT P.person_id, P.first_name, P.last_name, P.gender
//       FROM Relationship AS R
//       INNER JOIN Person AS P ON R.child_id = P.person_id
//       WHERE R.parent_id = ? AND (R.relationship_type = 'son' OR R.relationship_type = 'daughter')
//     `;
//     const [childrenResult] = await connection.query(getChildrenSql, [personId]);

//     // Retrieve the person's spouse
//     // const getSpouseSql = `SELECT P.person_id, P.first_name, P.last_name, P.gender
//     //   FROM Relationship AS R
//     //   INNER JOIN Person AS P ON (R.parent_id = P.person_id OR R.child_id = P.person_id)
//     //   WHERE R.parent_id = ? AND R.relationship_type = 'spouse'`;

//     const getSpouseSql = `select parent_id from relationship where child_id = ?`

//     let [spouseResult] = await connection.query(getSpouseSql, [personId]);

//     spouseResult = await connection.query(`select * from parent where person_id = ${spouseResult[o].parent_id}`);

//     // Construct the family tree object
//     const familyTree = {
//       person,
//       parents: parentsResult,
//       children: childrenResult,
//       spouse: spouseResult.length > 0 ? spouseResult[0] : null, // Use null if no spouse is found
//     };

//     res.status(200).json(familyTree);
//   } catch (err) {
//     console.error('Error retrieving family tree:', err);
//     res.status(500).json({ error: 'Unable to retrieve family tree' });
//   } finally {
//     connection.release();
//   }
// };


exports.getFamilyTree = async (req, res) => {
  const personId = req.params.person_id;

  const connection = await pool.getConnection();

  try {
    // Retrieve the person's details
    const getPersonSql = 'SELECT * FROM Person WHERE person_id = ?';
    const [personResult] = await connection.query(getPersonSql, [personId]);

    if (personResult.length === 0) {
      res.status(404).json({ error: 'Person not found' });
      return;
    }

    const person = personResult[0];

    // Retrieve the person's parents
    const getParentsSql = `
      SELECT P.person_id, P.first_name, P.last_name, P.gender
      FROM Relationship AS R
      INNER JOIN Person AS P ON R.parent_id = P.person_id
      WHERE R.child_id = ? AND (R.relationship_type = 'son' OR R.relationship_type = 'daughter')
    `;
    const [parentsResult] = await connection.query(getParentsSql, [personId]);

    // Retrieve the person's children
    const getChildrenSql = `
      SELECT P.person_id, P.first_name, P.last_name, P.gender
      FROM Relationship AS R
      INNER JOIN Person AS P ON R.child_id = P.person_id
      WHERE R.parent_id = ? AND (R.relationship_type = 'son' OR R.relationship_type = 'daughter')
    `;
    const [childrenResult] = await connection.query(getChildrenSql, [personId]);

    // Retrieve the person's spouse
    const getSpouseSql = `select parent_id from relationship where child_id = ? AND relationship_type= "spouse"`;

    let [spouseResult] = await connection.query(getSpouseSql, [personId]);

    if (spouseResult.length > 0) {
      spouseResult = await connection.query(`select * from person where person_id = ${spouseResult[0].parent_id}`);
    } else {
      spouseResult = []; // Set it to an empty array if no spouse is found
    }

    // Construct the family tree object
    const familyTree = {
      person,
      parents: parentsResult,
      children: childrenResult,
      spouse: spouseResult.length > 0 ? spouseResult[0] : null, // Use null if no spouse is found
    };

    res.status(200).json(familyTree);
  } catch (err) {
    console.error('Error retrieving family tree:', err);
    res.status(500).json({ error: 'Unable to retrieve family tree' });
  } finally {
    connection.release();
  }
};



exports.getAllUsers = async (req, res) => {
  const connection = await pool.getConnection();
  const sql = 'select * from person';

  try {
    const [result] = await connection.query(sql);
    console.log("result",result)
    res.status(200).json({ status: "success", data: result });
  } catch (err) {
    res.status(500).json({ error: 'unable to crate user'  });
  } finally {
    connection.release();
  }
};




