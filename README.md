# Server

> Master server for the adhouz

## Steps:-

- Download .zip
- cd Server

- run _"yarn add"_ in terminal.
- run _"yarn start"_ in terminal to run in production mode.
- run _"yarn dev"_ in terminal to run in development mode.

Server will start on localhost:5000 by default ,port can be changes from server.js

---

**Now you are all ready to use APi**

---
