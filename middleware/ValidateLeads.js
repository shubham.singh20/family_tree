const Validator = require('validator')
const isEmpty = require('is-empty')

module.exports = ValidateLeads = data => {
  const errors = {}
  if (isEmpty(data)) {
    return {
      errors: { emptyData: '{} data requested' },
      isValid: false,
    }
  }
  // Convert empty fields to an empty string so we can use validator functions
  data.email = !isEmpty(data.email) ? data.email : ''
  data.phone_number = !isEmpty(data.phone_number) ? data.phone_number : ''

  // Email checks
  if (Validator.isEmpty(data.email)) {
    errors.email = 'Email field is required'
  } else if (!Validator.isEmail(data.email)) {
    errors.email = 'Email is invalid'
  }

  // Password checks
  if (Validator.isEmpty(data.phone_number)) {
    errors.phone_number = 'phone number field is required'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
