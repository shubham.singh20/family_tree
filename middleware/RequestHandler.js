const multer = require('multer')

// File upload filter to only upload image file
const filterExt = (req, file, callback) => {
  const fileType = file.mimetype.split('/')[0]
  if (fileType === 'image') {
    // Check file extension
    const validExtensions = ['jpg', 'jpeg', 'png', 'gif']
    const fileExtension = file.originalname.split('.').pop().toLowerCase()
    if (validExtensions.includes(fileExtension)) {
      callback(null, true)
    } else {
      // Remove uploaded file if extension doesn't match
      req.fileValidationError = 'Invalid file extension'
      callback(null, false)
    }
  } else {
    callback(new Error('Only image formats allowed!'))
  }
}

// File upload error handling middleware
const handleFileUploadError = (err, req, res, next) => {
  if (err instanceof multer.MulterError) {
    // Multer error occurred during file upload
    res.status(400).json({
      success: false,
      message: 'File upload error',
      error: err.message,
    })
  } else if (err) {
    // Other error occurred during file upload
    res.status(500).json({
      success: false,
      message: 'Internal Server Error',
      error: err.message,
    })
  } else {
    next()
  }
}

// Middleware for handling pagination limits and page numbers
const limitPagination = (req, res, next) => {
  const paramData = req.query

  // Set the default limit to 12 or use the limit value from the request query
  let limit = Number(paramData.limit) || 12

  let page_number = 0
  if (paramData.page_number) {
    // Calculate the starting index for pagination based on the page number and limit
    if (paramData.page_number > 1) {
      page_number = (paramData.page_number - 1) * limit
    } else {
      page_number = 0
    }
  } else {
    page_number = 0
  }

  // Limit the maximum value of the limit to 50
  if (limit > 50) {
    limit = 50
  }

  // Update the request query object with the modified limit and page_number values
  req.query = {
    ...paramData,
    limit,
    page_number,
  }

  next()
}

module.exports = {
  filterExt,
  limitPagination,
  handleFileUploadError,
}
