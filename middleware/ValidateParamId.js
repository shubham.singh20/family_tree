const Validator = require('validator')
const isEmpty = require('is-empty')

module.exports = ValidateParamId = id => {
  const errors = {}

  // Convert empty fields to an empty string so we can use validator functions
  id = !isEmpty(id) ? id : ''

  // Check id
  if (Validator.isEmpty(id)) {
    errors.id = 'Id is required'
  } else if (!Validator.isNumeric(id)) {
    errors.id = 'Id is invalid'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
