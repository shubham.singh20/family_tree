const Validator = require('validator')
const isEmpty = require('is-empty')

module.exports = ValidateCartInput = data => {
  const errors = {}

  if (isEmpty(data)) {
    return {
      errors: { emptyData: '{} data requested' },
      isValid: false,
    }
  }

  // Convert empty fields to an empty string so we can use validator functions
  data.user_id = !isEmpty(data.user_id) ? data.user_id : ''
  data.customization = !isEmpty(data.customization) ? data.customization : ''
  data.service_type = !isEmpty(data.service_type) ? data.service_type : ''
  data.service_id = !isEmpty(data.service_id) ? data.service_id : ''
  data.notes = !isEmpty(data.notes) ? data.notes : ''
  data.special_request = !isEmpty(data.special_request)
    ? data.special_request
    : 0

  if (Validator.isEmpty(data.user_id)) {
    errors.user_id = 'User id is required'
  } else if (!Validator.isNumeric(data.user_id)) {
    errors.user_id = 'User id is Incorrect'
  }

  if (Validator.isEmpty(data.customization)) {
    errors.customization = 'Customization is required'
  } else if (
    data.customization.length <= 10 ||
    data.customization.length >= 250
  ) {
    errors.customizationLength = 'Customization data has an invalid length'
  }
  // else if(!data.customization.replace('"', '').ad_length){
  //   errors.customization = 'ad_length is required'
  // }else if(!data.customization.replace('"', '').played_perday){
  //   errors.customization = 'played_perday is required'
  // }else if(!data.customization.replace('"', '').total_days){
  //   errors.customization = 'total_days is required'
  // }else if(!data.customization.replace('"', '').time_band){
  //   errors.customization = 'time_band is required'
  // }

  if (Validator.isEmpty(data.service_type)) {
    errors.service_type = 'Service type is required'
  }

  if (Validator.isEmpty(data.service_id)) {
    errors.service_id = 'Service id is required'
  } else if (!Validator.isNumeric(data.service_id)) {
    errors.service_id = 'Service id is Incorrect'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
