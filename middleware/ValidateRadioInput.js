const Validator = require('validator')
const isEmpty = require('is-empty')

module.exports = ValidateRadioInput = data => {
  const errors = {}

  if (isEmpty(data)) {
    return {
      errors: { emptyData: '{} data requested' },
      isValid: false,
    }
  }

  // Convert empty fields to an empty string so we can use validator functions
  data.name = !isEmpty(data.name) ? data.name : ''
  data.type = !isEmpty(data.type) ? data.type : ''
  data.language = !isEmpty(data.language) ? data.language : ''
  data.geo_location = !isEmpty(data.geo_location) ? data.geo_location : ''
  data.attributes = !isEmpty(data.attributes) ? data.attributes : ''
  data.time_band = !isEmpty(data.time_band) ? data.time_band : ''

  if (Validator.isEmpty(data.name)) {
    errors.name = 'Radio name field is required'
  }

  if (Validator.isEmpty(data.type)) {
    errors.type = 'Media Type field is required'
  }

  if (Validator.isEmpty(data.language)) {
    errors.language = 'Language field is required'
  }

  if (Validator.isEmpty(data.geo_location)) {
    errors.geo_location = 'Geo location field is required'
  }

  if (Validator.isEmpty(data.attributes)) {
    errors.attributes = 'Attributes field is required'
  }

  if (Validator.isEmpty(data.time_band)) {
    errors.time_band = 'Time band field is required'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
