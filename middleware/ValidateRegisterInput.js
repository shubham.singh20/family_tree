const Validator = require('validator')
const isEmpty = require('is-empty')

module.exports = ValidateRegisterInput = data => {
  const errors = {}
  const emailRegex =
    /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)\b/
  if (isEmpty(data)) {
    return {
      errors: { emptyData: '{} data requested' },
      isValid: false,
    }
  }
  // Convert empty fields to an empty string so we can use validator functions
  data.first_name = !isEmpty(data.first_name) ? data.first_name : ''
  data.last_name = !isEmpty(data.last_name) ? data.last_name : ''
  data.email = !isEmpty(data.email) ? data.email : ''
  data.password = !isEmpty(data.password) ? data.password : ''
  data.phone_number = !isEmpty(data.phone_number) ? data.phone_number : ''
  data.gst_number = !isEmpty(data.gst_number) ? data.gst_number : null
  data.state = !isEmpty(data.state) ? data.state : null
  data.city = !isEmpty(data.city) ? data.city : null
  data.pincode = !isEmpty(data.pincode) ? data.pincode : null

  // Name checks
  if (Validator.isEmpty(data.first_name)) {
    errors.first_name = 'First name field is required'
  } else if (!Validator.isAlpha(data.first_name.trim())) {
    errors.first_name = 'First name should only contain alphabets'
  }

  if (Validator.isEmpty(data.last_name)) {
    errors.last_name = 'Last name field is required'
  } else if (!Validator.isAlpha(data.last_name.trim())) {
    errors.last_name = 'Last name should only contain alphabets'
  }

  // Email checks
  if (Validator.isEmpty(data.email)) {
    errors.email = 'Email field is required'
  } else if (!Validator.isEmail(data.email)) {
    errors.email = 'Email is invalid'
  } else if (!Validator.matches(data.email, emailRegex)) {
    errors.email = 'Email format is invalid'
  }

  // Password checks
  if (Validator.isEmpty(data.password)) {
    errors.password = 'Password field is required'
  } else if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = 'Password must be between 6 and 30 characters'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
