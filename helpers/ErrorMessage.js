module.exports = ErrorMessage = error => {
  let data

  if (error && error.kind && error.kind === 'ObjectId') {
    data = {
      success: false,
      message: `No record found with id: ${error.value.id || error.value}.`,
    }
  } else if (error === 'invalidToken') {
    data = {
      success: false,
      message: 'Invalid Token: You have passed an invalid token.',
    }
  } else if (error === 'noToken') {
    data = {
      success: false,
      message: 'Invalid Token: You have not passed any token.',
    }
  } else if (error === 'emptyString') {
    data = {
      success: false,
      message: 'Empty String: You have passed incomplete data.',
    }
  } else if (error === 'invalidOldPassword') {
    data = {
      success: false,
      message:
        'Invalid Password: You have entered the old password incorrectly.',
    }
  } else if (error === 'userNotFound') {
    data = {
      success: false,
      message: 'User Not Found: The specified user does not exist.',
    }
  } else if (error === 'unauthorizedAccess') {
    data = {
      success: false,
      message:
        'Unauthorized Access: You do not have permission to perform this action.',
    }
  } else {
    data = {
      success: false,
      message: error,
    }
  }

  return data
}
