module.exports = {
  // User Queries
  GET_ALL_USER_COUNT: 'SELECT COUNT(*) as total_rows FROM users',
  GET_ALL_USER:
    'SELECT id,first_name,last_name,email,phone_number,profile_pic,address,state,city,pincode,gst_number,company,deleted FROM users',
  GET_USERS_DISTINCT_CITIES:
    'SELECT DISTINCT(city) FROM users WHERE NOT city = "" ORDER BY city',
  GET_USERS_DISTINCT_STATES:
    'SELECT DISTINCT state FROM users WHERE NOT state = ""  ORDER BY state',
  GET_USERS_LIST: 'SELECT id,first_name,last_name FROM users',
  GET_SINGLE_USER:
    'SELECT id,first_name,last_name,email,phone_number,profile_pic,address,state,city,pincode,gst_number,company FROM users WHERE id = ?',
  FIND_EMAIL_EXIST: 'SELECT id,email FROM users WHERE email=?',
  LOGIN_USER: 'SELECT * FROM users WHERE email = ?',
  CREATE_USER: 'INSERT INTO `users` SET ?',
  DELETE_USER: 'UPDATE `users` SET deleted=? WHERE id = ?',
  UPDATE_USER: 'UPDATE `users` SET ? WHERE `id`=? AND deleted=0',

  // Radio Queries
  GET_ALL_RADIO_COUNT: 'SELECT COUNT(*) as total_rows FROM radios',
  GET_RADIO_DATA: 'SELECT * FROM radios',
  GET_RADIO_LOGOS: 'SELECT DISTINCT(logo) FROM radios',
  GET_RADIO_DISTINCT_LANGUAGES:
    "SELECT DISTINCT JSON_UNQUOTE(JSON_EXTRACT(language, '$[0]')) AS language FROM radios",
  GET_RADIO_DISTINCT_CITIES:
    "SELECT DISTINCT JSON_UNQUOTE(JSON_EXTRACT(geo_location, '$.city')) AS city FROM radios ORDER BY city",
  GET_RADIO_DISTINCT_TYPE: 'SELECT DISTINCT type FROM radios ORDER BY type',
  GET_SINGLE_RADIO: 'SELECT * FROM radios WHERE id = ?',
  GET_RADIO_BY_NAME: 'SELECT * FROM radios WHERE name = ?',
  DELETE_RADIO: 'UPDATE `radios` SET deleted=? WHERE id = ?',
  CREATE_RADIO:
    'INSERT INTO `radios`(`name`, `type`, `geo_location`, `logo`, `sample_media`, `attributes`, `language`, `time_band`) VALUES (?)',
  CREATE_RADIO_NEW:
    'INSERT INTO `radios`(`name`, `type`, `logo`, `sample_media`) VALUES (?)',
  UPDATE_RADIO_BY_ID: 'UPDATE `radios` SET ? where `id`=? AND `deleted`=0',
  UPDATE_RADIO_BY_NAME: 'UPDATE `radios` SET ? where `name`=? AND `deleted`=0',

  // Cinema Queries
  GET_ALL_CINEMA_COUNT: 'SELECT COUNT(*) as total_rows FROM cinema',
  GET_CINEMA_DATA: 'SELECT * FROM cinema',
  GET_CINEMA_LOGOS: 'SELECT DISTINCT(logo) FROM cinema',
  GET_CINEMA_DISTINCT_LANGUAGES:
    "SELECT DISTINCT JSON_UNQUOTE(JSON_EXTRACT(language, CONCAT('$[', nums.n, ']'))) AS language FROM (SELECT 0 AS n UNION ALL SELECT 1 UNION ALL SELECT 2) AS nums JOIN cinema ON JSON_LENGTH(language) > nums.n WHERE nums.n < JSON_LENGTH(language)",
  GET_CINEMA_DISTINCT_CITIES:
    "SELECT DISTINCT JSON_UNQUOTE(JSON_EXTRACT(geo_location, '$.city')) AS city FROM cinema ORDER BY city",
  GET_CINEMA_DISTINCT_TYPE:
    "SELECT DISTINCT JSON_UNQUOTE(JSON_EXTRACT(type, '$.type')) AS type FROM cinema ORDER BY type",
  GET_SINGLE_CINEMA: 'SELECT * FROM cinema WHERE id = ?',
  GET_CINEMA_BY_NAME: 'SELECT * FROM cinema WHERE name = ?',
  DELETE_CINEMA: 'UPDATE `cinema` SET deleted=? WHERE id = ?',
  CREATE_CINEMA:
    'INSERT INTO `cinema`(`name`, `type`, `geo_location`, `logo`, `sample_media`, `attributes`, `language`, `time_band`) VALUES (?)',
  UPDATE_CINEMA_BY_ID: 'UPDATE `cinema` SET ? where `id`=? AND `deleted`=0',
  UPDATE_CINEMA_BY_NAME: 'UPDATE `cinema` SET ? where `name`=? AND `deleted`=0',

  // LEADS Queries
  GET_RADIO_LEADS:
    'SELECT * from radios WHERE city=? AND language=? AND name=?',
  GET_NEWSPAPER_LEADS:
    'SELECT * from newspaper WHERE city=? AND newspaper=? AND category=?',
  GET_TELEVISION_LEADS:
    'SELECT * from television WHERE channel=? AND language=? AND category=?',

  // Carts Queries
  GET_ALL_CARTS_COUNT: 'SELECT COUNT(*) as total_rows FROM cart',
  GET_CARTS_DATA:
    'SELECT cart.*,radios.name,radios.geo_location FROM cart INNER JOIN radios ON cart.service_id=radios.id',
  GET_USER_CARTS_COUNT:
    'SELECT COUNT(*) as total_rows FROM cart WHERE user_id=?',
  GET_CART_DISTINCT_STATUS:
    'SELECT DISTINCT(cart_status) FROM `cart` ORDER BY cart_status',
  GET_CART_DISTINCT_SERVICE_ID:
    'SELECT DISTINCT(service_id),radios.name FROM `cart` INNER JOIN radios ON cart.service_id=radios.id ORDER BY cart.service_id',
  GET_CART_DISTINCT_USERS:
    'SELECT DISTINCT(user_id),users.first_name,users.last_name FROM `cart` INNER JOIN users ON cart.user_id=users.id ORDER BY cart.user_id',
  GET_USER_CARTS:
    'SELECT cart.cart_id, cart.customization, radios.* FROM cart INNER JOIN radios ON cart.service_id=radios.id WHERE user_id=? AND cart.deleted=0',
  GET_INVOICE_CARTS:
    'SELECT cart.service_type, cart.customization, radios.name FROM cart INNER JOIN radios ON cart.service_id=radios.id WHERE user_id=? AND cart.deleted=0',
  GET_AVAILABLE_CART:
    'SELECT * FROM cart WHERE user_id = ? AND service_id = ? AND deleted=0',
  UPDATE_CART_BY_ID: 'UPDATE `cart` SET ? where `cart_id`=?',
  UPDATE_CART_BY_USER_ID: 'UPDATE `cart` SET ? where deleted=0 AND `user_id`=?',
  CREATE_CART:
    'INSERT INTO `cart`(`user_id`, `customization`, `service_type_id`, `service_type`, `service_id`, `tax_rates`, `notes`, `special_request`, `cart_status`)  VALUES (?)',
  DELETE_CART:
    'UPDATE `cart` SET deleted=?,cart_status=? WHERE cart_id = ? AND user_id = ?',

  // Orders Queries
  GET_ALL_ORDERS_COUNT: 'SELECT COUNT(*) as total_rows FROM orders',
  GET_CARTS_ORDER_COUNT:
    'SELECT COUNT(*) as total_rows FROM orders WHERE user_id=?',
  GET_ALL_ORDERS_DATA: 'SELECT * FROM orders',
  GET_CARTS_ORDER_DATA: 'SELECT * FROM orders WHERE user_id=?',
  GET_LAST_INVOICE_ID_USED:
    'SELECT orders.invoice_id FROM orders ORDER BY invoice_id DESC LIMIT 1',
  GET_ORDER_INVOICE_STATUS:
    'SELECT payment_status FROM orders WHERE invoice_id = ?',
  CREATE_ORDER:
    'INSERT INTO `orders`(`invoice_id`, `booking_date`, `payment_status`, `price`, `user_id`)  VALUES (?)',
  UPDATE_ORDER_BY_ID: 'UPDATE `orders` SET ? where `id`=? AND invoice_id=?',

  // Invoices Queries
  GET_ORDERS_INVOICE_COUNT:
    'SELECT COUNT(*) as total_rows FROM invoices WHERE invoice_id=?',
  GET_ORDERS_INVOICE_DATA: 'SELECT * FROM invoices WHERE invoice_id=?',
  CREATE_INVOICE:
    'INSERT INTO `invoices`(`invoice_id`, `media_name`, `media_type`, `campaign`, `amount`)  VALUES (?)',
  UPDATE_INVOICE_BY_ID: 'UPDATE `invoices` SET ? where `id`=? AND invoice_id=?',
}
